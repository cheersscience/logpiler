const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

const config = require('../utils/config');
const User = require('../db/models/User');

const loginRouter = require('express').Router();

// Returns a token and the username for a valid email and password combination
loginRouter.post('/', async (request, response) => {

   // TODO test whether leading and/or trailing whitespace in the email makes a
   // difference
   const user = await User.findOne({ where: { email: request.body.email} });

   let passwordCorrect = false;
   if (user) {
      passwordCorrect = await bcrypt.compare(
         request.body.password, user.passwordHash
      );
   }

   if (!user || !passwordCorrect) {
      return response.status(401).json({
         message: 'Invalid email or password!'
      });
   }

   const userForToken = {
      name: user.name,
      email: user.email
   };

   const token = jwt.sign(userForToken, config.SECRET, { expiresIn: '8h' });

   response.status(200).send({
      userName: user.name, token
   });
});

module.exports = loginRouter