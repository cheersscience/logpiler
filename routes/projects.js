const Project = require('../db/models/Project');

const sequelize = require('../db/models/Sequelize');

const projectsRouter = require('express').Router();

projectsRouter
   // Get an individual project
   .get('/:projectName', async (request, response, next) => {
      const projectName = request.params.projectName;

      try {
         const projectInDb = await Project.findOne({
            where: { name: projectName }
         });

         if (!projectInDb) {
            return response.status(404).json({
               error: `could not find project "${projectName}"` });
         }

         response.json(projectInDb);

      } catch (error) {
         console.error(`error while getting project "${projectName}"`);
         next(error);
      }
   })
   // Get all projects that the current user is assigned to together with the
   // total pile volume for each project
   .get('/of/user', async (request, response, next) => {
      const userName = request.decodedUserToken.name;

      try {
         const projectsInDb = await sequelize.query(
            `WITH
               Worker_project_ids AS
                  (SELECT project_id
                     FROM Worker_x_Project WHERE worker_id = $1),
               Projects_volumes AS
                  (SELECT project_id, sum(volume) AS total_volume
                     FROM Pile GROUP BY project_id)
            SELECT
               project_id,
               start_date,
               end_date,
               target_volume,
               total_volume AS "totalVolume"
            FROM Worker_project_ids
               LEFT JOIN Project USING (project_id)
               LEFT JOIN Projects_volumes USING (project_id)`,
            {
               bind: [userName],
               model: Project,
               mapToModel: true
            }
         );

         response.json(projectsInDb);

      } catch (error) {
         console.error(
            `error while getting the projects of user "${userName}"`);
         next(error);
      }
   })
   // Update an individual project
   .put('/:projectName', async (
      request, response, next
   ) => {
      const projectName = request.params.projectName;
      const requestedUpdate = request.body;

      try {

         const updatedProject = await Project.update(
            {
               startDate: requestedUpdate.startDate,
               endDate: requestedUpdate.endDate,
               targetVolume: requestedUpdate.targetVolume
                  ? requestedUpdate.targetVolume
                  : null
            },
            {
               where: { name: projectName },
               returning: true
            }
         );

         console.log(JSON.stringify(updatedProject));

         if (!updatedProject[0] === 0) {
            return response.status(404).json({ error: 'project not found' });
         }

         response.json(updatedProject[1][0]);

      } catch (error) {
         console.error(
            `error while updating the target volume of project
            "${projectName}" with ${targetVolume}`);
         next(error);
      }
   });

module.exports = projectsRouter;