const Pile = require('../db/models/Pile');

const pilesRouter = require('express').Router();

pilesRouter
   // Get all piles of a single project
   .get('/ofProject/:projectName', async (request, response, next) => {
      const projectName = request.params.projectName;

      try {

         const pilesInDb = await Pile.findAll(
            { where: { project: projectName } }
         );
         response.json(pilesInDb);

      } catch (error) {

         console.error(
            `error while getting the piles for project "${projectName}"`);
         next(error);

      }
   })
   // Add a pile to a project
   .post('/ofProject/:projectName', async (request, response, next) => {
      const projectName = request.params.projectName;
      const newPile = request.body;

      try {

         const pileInDb = await Pile.create({
            sortiment: newPile.sortiment,
            volume: newPile.volume,
            project: projectName,
            user: request.decodedUserToken.name,
            location: newPile.location
         });

         console.log(pileInDb);

         response.json(pileInDb);

      } catch (error) {

         if (error.name === 'SequelizeValidationError') {
            return response.status(400).json({
               error: 'invalid object format',
               details: error.errors
            });
         }

         console.error(
            `error while storing the following pile for project
            "${projectName}":`,
            JSON.stringify(newPile)
         );
         console.error(JSON.stringify(error));
         next(error);
      }
   })
   // Delete individual piles
   .delete('/:id(\\d+)', async (request, response, next) => {
      const userName = request.decodedUserToken.name;
      const pileId = request.params.id;

      try {

         const pileInDb = await Pile.findByPk(pileId);

         if (!pileInDb) {
            return response.status(200).json({
               message: `could not find pile with id ${pileId}`
            });
         }

         if (pileInDb.user !== userName) {
            return response.status(401).json({
               error: `user "${userName}" not authorized to delete requested
                  pile`
            });
         }

         await pileInDb.destroy();

         response.status(200).send();

      } catch (error) {

         console.error(`error while deleting the pile with id "${pileId}"`);
         next(error);

      }
   });

module.exports = pilesRouter;