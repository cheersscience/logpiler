const bcrypt = require('bcrypt');

const User = require('../db/models/User');

const usersRouter = require('express').Router();

usersRouter
   // Register a user
   .post('/', async (request, response, next) => {
      try {
         const passwordHash = await bcrypt.hash(request.body.password, 12);

         const userInDb = await User.create({
            name: request.body.name,
            email: request.body.email,
            passwordHash: passwordHash
         });

         console.log(userInDb.toJSON());

      } catch (error) {
         console.error(
            `Error while registering the following user:
            ${JSON.stringify(request.body)}`);
         next(error);
      }
   })
   // Delete a user
   .delete('/:name', async (request, response, next) => {
      const loggedInUser = request.decodedUserToken.name;
      const toBeDeletedUser = request.params.name;

      if (loggedInUser !== toBeDeletedUser) {
         return response.status(401).send();
      }

      // TODO: When a user is deleted, at least some of their data should be
      //    retained, i.e. no pile should ever loose the information on who
      //    recorded it.

      try {
         const result = await User.destroy({
            where: { name: natoBeDeletedUserme }
         })
         response.send(`deleted user ${toBeDeletedUser}`);
      } catch (error) {
         console.error(
            `error while deleteing user ${toBeDeletedUser}:
            ${JSON.stringify(error)}`);
         next(error);
      }
   });

module.exports = usersRouter;