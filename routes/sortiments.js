const Sortiment = require('../db/models/Sortiment');

const sortimentsRouter = require('express').Router();

// Get all available timber sortimens
sortimentsRouter.get('/', async (request, response, next) => {

   try {

      const sortimentsInDb = await Sortiment.findAll();
      response.json(sortimentsInDb);

   } catch (error) {

      console.error(`error while getting timber sortiments`);
      next(error);

   }
});

module.exports = sortimentsRouter;