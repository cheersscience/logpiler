const jwt = require('jsonwebtoken');
const config = require('../config');

// Extracts a bearer token from the authorization header
function getTokenFrom(request) {
   const authorization = request.get('authorization');

   if (authorization && authorization.toLowerCase().startsWith('bearer ')) {
      return authorization.substring(7);
   } else {
      return null;
   }
}

// Decodes a user token and makes the result available to follow-up middleware.
// Ends the request-response-cycle with a 401 if the token is missing or
// invalid.
function decodeUserToken(request, response, next) {
   const token = getTokenFrom(request);

   let decodedToken;
   try {

      decodedToken = token
         ? jwt.verify(token, config.SECRET)
         : null;

   } catch (error) {

      if (
         // For a full list of possible errors see
         // https://github.com/auth0/node-jsonwebtoken#errors--codes
         error.name === 'JsonWebTokenError' ||
         error.name === 'TokenExpiredError'
      ) {
         console.log(
            `There was an invalid token! error: ${JSON.stringify(error)}`);
         decodedToken = null;
      } else {
         console.error(
            `Error while verifying token ${JSON.stringify(token)}`);
         console.error(JSON.stringify(error));
         next(error);
      }
   }

   if (!decodedToken || !decodedToken.name) {
      return response.status(401).json({
         error: 'Token missing or invalid '});
   }

   request.decodedUserToken = decodedToken;
   next();
}

module.exports = decodeUserToken;