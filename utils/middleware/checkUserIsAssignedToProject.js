const UserXProject = require('../../db/models/UserXProject');

async function checkUserIsAssignedToProject(request, response, next) {
   console.log('Checking that user is assigned to requested project...');

   const userName = request.decodedUserToken.name;
   const projectName = request.params.projectName;

   try {
      const assignment = await UserXProject.findOne({
         where: { user: userName, project: projectName }
      });

      if (!assignment) {
         return response.status(401).json({
            error: `user "${userName}" not authorized to access project
               ${projectName}`
         });
      }
   } catch (error) {
      console.error(`error while getting possible assignment of user
         "${userName}" to project ${projectName}`);
      next(error);
   }

   next();
}

module.exports = checkUserIsAssignedToProject;