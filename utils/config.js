require('dotenv').config();

const process = require('process');

const PORT = process.env.PORT;
const DATABASE_URL = process.env.DATABASE_URL;
const SECRET = process.env.SECRET;

module.exports = { PORT, DATABASE_URL, SECRET };
