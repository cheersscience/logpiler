// format for the number indicating project progress
const progressFormat = new Intl.NumberFormat(
   'en-US', { style: 'percent' }
);

const ProjectListElement = ({ project, switchToProject }) => {

   // Determine whether the project is currently ongoing
   const today = new Date();
   const ongoing =
      project.startDate < today && today < project.endDate;

   // Format the project's time range display
   let projectTime;
   if (ongoing) {
      projectTime = <>
         ongoing until <br/>
         {project.endDate.toLocaleDateString()}
      </>;
   } else {
      projectTime = <>
         {`${project.startDate.toLocaleDateString()}`} <br/>
         {`- ${project.endDate.toLocaleDateString()}`}
      </>;
   }

   return <li className={ongoing ? 'ongoing' : ''} >
      <div className='wrapper'>
         <button
            type='button'
            onClick={() => switchToProject(project.name)}
         >
            {project.name}
         </button>
         <div>
            {project.targetVolume && progressFormat.format(
               project.totalVolume / project.targetVolume
            )}
         </div>
         <div> {projectTime} </div>
      </div>
   </li>;
};

export default ProjectListElement;