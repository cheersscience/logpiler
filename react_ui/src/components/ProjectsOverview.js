import { useEffect } from 'react';

import './ProjectsOverview.css';

import sortimentService from '../services/sortiments';
import projectService from '../services/projects';

import ProjectListElement from './ProjectsOverview/ProjectListElement';

const ProjectsOverview = ({
   user, setUser,
   setSortiments,
   projects, setProjects,
   setCurrentProject,
   setCurrentPage,
}) => {

   // Load timber sortiments
   useEffect(() => {(async () => {
      const sortimentsOnServer = await sortimentService.getSortiments(user);
      setSortiments(sortimentsOnServer);
   })();
   }, [user, setSortiments]);

   // Load projects of current user
   useEffect(() => {(async () => {
      const currentUsersProjects = await projectService.getProjectsOf(user)
      setProjects(currentUsersProjects);
   })();
   }, [user, setProjects]);

   // to be executed when the users clicks a project in the list
   async function switchToProject(projectName) {
      const project = await projectService.getProject(projectName, user);
      setCurrentProject(project);
      setCurrentPage('ProjectView');
   }

   return <>
   <header>
      <div className='wrapper'>
         <h1> Your Projects </h1>
         <button
            type='button'
            onClick={() => {
               setCurrentPage('Login');
               setUser(null);
            }}
         >
            Log out
         </button>
      </div>
   </header>
   <main>
      <nav>
         <ul id='project-list'>
            {projects.length === 0
               ? <p> You have not been assigned to any projects yet </p>
               : projects
                  // Make future/most recent projects appear at the top of the
                  // list by sorting them backwards by their start date
                  .sort((a, b) => {
                     if (a.startDate > b.startDate) { return -1; }
                     if (a.startDate < b.startDate) { return 1; }
                     return 0;
                  })
                  // Create a list element for every project
                  .map(project => (
                     <ProjectListElement
                        key={project.name}
                        project={project}
                        switchToProject={switchToProject}
                     />
                  ))
            }
         </ul>
      </nav>
   </main>
   </>;
};

export default ProjectsOverview;