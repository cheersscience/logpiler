import { useState, useEffect } from "react";

import elements from "./PileSize/Elements";

const PileSize = ({
   sortiment,
   sizeUnit,
   setSizeUnit,
   setNetVolume
}) => {

   const [volume, setVolume] = useState('');
   const [count, setCount] = useState('');
   const [pileFrontArea, setPileFrontArea] = useState('');

   // Calculate the net volume based on the currently open input and it's value.
   // One special effect here: any input for log count or pile front area
   // generates a net volume estimate which is put into the volume input where
   // it can be modified.
   useEffect(() => {

      // The following format and function are used for rounding volume values
      // that are calculated from log counts or pile front areas
      const volumeFormat = new Intl.NumberFormat(
         'en', { useGrouping:false, maximumFractionDigits: 2 }
      );
      function setVolumeRounded(newValue) {
         const newNumber = Number(newValue);
         if (Number.isFinite(newNumber) && newNumber > 0) {
            setVolume(volumeFormat.format(newNumber));
         } else {
            setVolume(newValue);
         }
      }

      switch (sizeUnit) {
         case 'volume':
            const volumeNumber = Number(volume);
            Number.isFinite(volumeNumber) && volumeNumber > 0
               ? setNetVolume(volumeNumber)
               : setNetVolume('');
            break;
         case 'count':
            if (!sortiment.volumePerLog) {
               setSizeUnit('volume');
               break;
            }
            const countNumber = Number(count);
            if (Number.isFinite(countNumber) && countNumber > 0) {
               const newVolume = countNumber * sortiment.volumePerLog;
               setNetVolume(newVolume);
               setVolumeRounded(newVolume);
            } else {
               setNetVolume('');
            }
            break;
         case 'pileFrontArea':
            if (!sortiment.numLogsPerPileFrontArea) {
               setSizeUnit('volume');
               break;
            }
            const pileFrontAreaNumber = Number(pileFrontArea);
            if (
               Number.isFinite(pileFrontAreaNumber) &&
               pileFrontAreaNumber > 0
            ) {
               const newVolume = pileFrontAreaNumber *
                  sortiment.numLogsPerPileFrontArea *
                  sortiment.volumePerLog;
               setNetVolume(newVolume);
               setVolumeRounded(newVolume);
            } else {
               setNetVolume('');
            }
            break;
         default:
            setNetVolume('');
            break;
      }
   }, [
      sizeUnit, setSizeUnit,
      volume, count, pileFrontArea,
      sortiment,
      setNetVolume
   ]);

   const volumeButton = <elements.volume.button
      setSizeUnit={setSizeUnit}
   />;
   const countButton = <elements.count.button
      setSizeUnit={setSizeUnit}
      disabled={!sortiment.volumePerLog}
   />;
   const pileFrontAreaButton = <elements.pileFrontArea.button
      setSizeUnit={setSizeUnit}
      disabled={!sortiment.numLogsPerPileFrontArea}
   />;

   if (sizeUnit === '') {
      return <>
         <label htmlFor='size'>Size:</label> <br/>
         <elements.volume.button id='size' setSizeUnit={setSizeUnit}/> {' or '}
         {countButton} {' or '} {pileFrontAreaButton}
      </>;
   }

   if (sizeUnit === 'volume') {
      return <>
         <label htmlFor='size'>Net volume:</label> <br/>
         <elements.volume.input
            id='size'
            volume={volume}
            setVolume={setVolume}
         /> <br/>
         {'or '} {countButton} {' or '} {pileFrontAreaButton}
      </>;
   }

   if (sizeUnit === 'count') {
      return <>
         <label htmlFor='size'>Log count:</label> <br/>
         <elements.count.input
            id='size'
            count={count}
            setCount={setCount}
         /> <br/>
         {'or '} {volumeButton} {' or '} {pileFrontAreaButton}
      </>;
   }

   if (sizeUnit === 'pileFrontArea') {
      return <>
         <label htmlFor='size'>Pile front area:</label> <br/>
         <elements.pileFrontArea.input
            id='size'
            pileFrontArea={pileFrontArea}
            setPileFrontArea={setPileFrontArea}
         /> <br/>
         {'or '} {volumeButton} {' or '} {countButton}
      </>;
   }

};

export default PileSize;