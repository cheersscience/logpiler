const VolumeButton = ({ setSizeUnit }) => {
   return <>
      <button
         type='button'
         onClick={() => setSizeUnit('volume')}
      >
         Net volume
      </button>
   </>;
};

const VolumeInput = ({ volume, setVolume }) => {
   return <>
      <input
         type='number'
         id='size'
         className='pile-size'
         step='any'
         inputMode='decimal'
         min='0'
         autoFocus
         value={volume}
         onChange={event => setVolume(event.target.value)}
      />
      {' m³'}
   </>;
};

const CountButton = ({ setSizeUnit, disabled }) => {
   return <>
      <button
         type='button'
         disabled={disabled}
         onClick={() => setSizeUnit('count')}
      >
         Log count
      </button>
   </>;
};

const CountInput = ({ count, setCount }) => {
   return <>
      <input
         type='number'
         id='size'
         className='pile-size'
         step='any'
         inputMode='decimal'
         min='0'
         autoFocus
         value={count}
         onChange={event => setCount(event.target.value)}
      />
      {' logs'}
   </>;
};

const PileFrontAreaButton = ({ setSizeUnit, disabled }) => {
   return <>
      <button
         type='button'
         disabled={disabled}
         onClick={() => setSizeUnit('pileFrontArea')}
      >
         Pile front area
      </button>
   </>;
};

const PileFrontAreaInput = ({ pileFrontArea, setPileFrontArea }) => {
   return <>
      <input
         type='number'
         id='size'
         className='pile-size'
         step='any'
         inputMode='decimal'
         min='0'
         autoFocus
         value={pileFrontArea}
         onChange={event => setPileFrontArea(event.target.value)}
      />
      {' m²'}
   </>;
};

const elements = {
   volume: {
      button: VolumeButton,
      input: VolumeInput
   },
   count: {
      button: CountButton,
      input: CountInput
   },
   pileFrontArea: {
      button: PileFrontAreaButton,
      input: PileFrontAreaInput
   }
}
export default elements;