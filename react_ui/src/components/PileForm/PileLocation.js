import { useState, useEffect } from "react";

// The checkbox for skipping the location input
const Skip = ({ skipLocation, setSkipLocation }) => {
   return <>
      <input
         id='skip-location'
         type='checkbox'
         checked={skipLocation}
         onChange={event => setSkipLocation(event.target.checked)}
      />
      <label htmlFor='skip-location'> {' Skip'} </label>
   </>;
};

// The button for showing the location input
const Choose = ({ skipLocation, setSkipLocation }) => {
   return <>
      <button
         id='location'
         type='button'
         onClick={() => setSkipLocation(false)}
      >
         Choose
      </button>
      {' '}
      <Skip skipLocation={skipLocation} setSkipLocation={setSkipLocation} />
   </>;
}

// The location input
const Input = ({
   xInput, setXInput,
   yInput, setYInput,
   skipLocation,
   setSkipLocation,
   setLocation,
}) => {

   return <>
      <label htmlFor='x'> {'X: '} </label>
      <input
         id='x'
         className='location-input'
         type='number'
         step='any'
         inputMode='decimal'
         autoFocus
         value={xInput}
         onChange={event => setXInput(event.target.value)}
      />
      <br/>
      <label htmlFor='y'> {'Y: '} </label>
      <input
         id='y'
         className='location-input'
         type='number'
         step='any'
         inputMode='decimal'
         value={yInput}
         onChange={event => setYInput(event.target.value)}
      />
      <br/>
      <Skip
         skipLocation={skipLocation}
         setSkipLocation={setSkipLocation}
      />
   </>;
};

const PileLocation = ({ setLocation, skipLocation, setSkipLocation }) => {

   const [xInput, setXInput] = useState('');
   const [yInput, setYInput] = useState('');

   useEffect(() => {

      const xNumber = Number(xInput);
      const yNumber = Number(yInput);

      if (
         !skipLocation &&
         xInput !== '' && yInput !== '' &&
         Number.isFinite(xNumber) && Number.isFinite(yNumber)
      ) {
         setLocation(`(${xNumber}, ${yNumber})`);
      } else {
         setLocation(null);
      }

   }, [skipLocation, xInput, yInput, setLocation]);

   return <>
      <label htmlFor='location'> Location: </label> <br/>
      {skipLocation
         ? <Choose
            skipLocation={skipLocation}
            setSkipLocation={setSkipLocation}
         />
         : <Input
            xInput={xInput} setXInput={setXInput}
            yInput={yInput} setYInput={setYInput}
            skipLocation={skipLocation}
            setSkipLocation={setSkipLocation}
            setLocation={setLocation}
         />
      }
   </>;
};

export default PileLocation;