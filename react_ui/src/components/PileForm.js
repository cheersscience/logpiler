import { useState } from 'react';

import './PileForm.css';
import PileLocation from './PileForm/PileLocation';

import PileSize from './PileForm/PileSize';

const PileForm = ({
   currentProject,
   setCurrentPage,
   addPile,
   sortiments,
   currentSortiment, setCurrentSortiment,
   sizeUnit, setSizeUnit,
   location, setLocation,
   skipLocation, setSkipLocation
}) => {
   const [netVolume, setNetVolume] = useState('');

   // Group the sortiments by species in a map object
   const groupedSortiments = new Map();
   sortiments.forEach(sortiment => {
      if (!groupedSortiments.has(sortiment.species)) {
         groupedSortiments.set(sortiment.species, []);
      }
      groupedSortiments.get(sortiment.species).push(sortiment);
   });

   // Create optgroups with option elements inside of them from the species map
   const sortimentSelectElements = [];
   groupedSortiments.forEach((sortiments, species) => {
      sortimentSelectElements.push(
         <optgroup key={species} label={species}>
            {sortiments.map(sortiment =>
               <option
                  key={sortiment.name}
                  value={sortiment.name}
               >
                  {sortiment.name}
               </option>
            )}
         </optgroup>
      );
   });

   return <>
   <header>
      <div className="wrapper">
         <label htmlFor='heading'> Harvesting Project: </label>
         <h1 id='heading'> {currentProject.name} </h1>
         <nav>
            <button
               type='button'
               onClick={() => setCurrentPage('ProjectView')}
            >
               Overview
            </button>
         </nav>
      </div>
   </header>
   <main>
      <form>
         <fieldset>
            <legend> Configure the new log pile </legend>
            <ul className='pile-input'>
               <li>
                  <label htmlFor='sortiment'>Sortiment:</label> <br/>
                  <select
                     id='sortiment'
                     value={currentSortiment.name}
                     onChange={event => {
                        setCurrentSortiment(
                           sortiments.find(
                              sortiment => sortiment.name === event.target.value
                           )
                        )
                     }}
                  >
                     {sortimentSelectElements}
                  </select>
               </li>
               <li>
                  <PileSize
                     sortiment={currentSortiment}
                     sizeUnit={sizeUnit} setSizeUnit={setSizeUnit}
                     setNetVolume={setNetVolume}
                  />
               </li>
               <li>
                  <PileLocation
                     setLocation={setLocation}
                     skipLocation={skipLocation}
                     setSkipLocation={setSkipLocation}
                  />
               </li>
            </ul>
            {netVolume === ''
               ? undefined
               : <dl className='pile-properties'>
                  <dt> Volume: </dt>
                  <dd>
                     <div> {Math.round(netVolume)} </div>
                     <div> m³ </div>
                  </dd>
                  <dt> Value: </dt>
                  <dd>
                     <div>
                        {Math.round(
                           netVolume * currentSortiment.valuePerVolume)}
                     </div>
                     <div> € </div>
                  </dd>
               </dl>
            }
            <button
               type='submit'
               className='main'
               id='add-pile'
               disabled={netVolume === '' || (!location && !skipLocation)}
               onClick={() => {
                  const newPile = {
                     sortiment: currentSortiment.name,
                     volume: netVolume,
                     location: location,
                     recordingTimestamp: new Date()
                  };
                  addPile(newPile);
                  setCurrentPage('ProjectView');
               }}
            >
               + Add pile
            </button>
         </fieldset>
      </form>
   </main>
   </>;
};

export default PileForm;