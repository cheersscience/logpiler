import { useState, useEffect } from 'react';

import './ProjectView.css';

import pileService from '../services/piles';

import PileForm from './PileForm';
import TargetSetter from './ProjectView/TargetSetter';

const ProjectView = ({
   user,
   sortiments,
   currentPage, setCurrentPage,
   project,
   setProjectTarget
}) => {

   const [piles, setPiles] = useState([]);

   // Hold several states for the PileForm here so that they are not lost
   // between subsequent additions of new log piles.
   const [currentSortiment, setCurrentSortiment] = useState(sortiments[0]);
   const [sizeUnit, setSizeUnit] = useState('');
   const [location, setLocation] = useState('');
   const [skipLocation, setSkipLocation] = useState(false);

   // Load the piles of the project
   useEffect(() => {(async () => {
      const currentProjectsPiles =
         await pileService.getPilesOf(project.name, user);
      setPiles(currentProjectsPiles);
   })();
   }, [project.name, user]);


   async function addPile(newPile) {
      const addedPile = await pileService.add(
         { ...newPile, project: project.name, user: user.name }, user
      );
      setPiles(piles.concat(addedPile));
   }

   async function deletePile(pile) {
      await pileService.deletePile(pile, user);
      setPiles(piles.filter(p => p !== pile));
   }


   if (currentPage === 'PileForm') {
      return <PileForm
         currentProject={project}
         setCurrentPage={setCurrentPage}
         addPile={addPile}
         sortiments={sortiments}
         currentSortiment={currentSortiment}
         setCurrentSortiment={setCurrentSortiment}
         sizeUnit={sizeUnit} setSizeUnit={setSizeUnit}
         location={location} setLocation={setLocation}
         skipLocation={skipLocation} setSkipLocation={setSkipLocation}
      />;
   }


   const totalVolume = piles.reduce((total, pile) => total + pile.volume, 0);
   const totalValue = piles.reduce((total, pile) => {
      const sortiment = sortiments.find(s => s.name === pile.sortiment);
      const pileValue = pile.volume * sortiment.valuePerVolume;
      return total + pileValue;
   }, 0);
   const relativeProgress = totalVolume / project.targetVolume;

   const volumeFormat = new Intl.NumberFormat(
      'en-US', { maximumFractionDigits: 0 }
   );
   const valueFormat = new Intl.NumberFormat(
      'en-US', { maximumFractionDigits: 0 }
   );
   const progressFormat = new Intl.NumberFormat(
      'en-US', { style: 'percent' }
   );

   return <>
   <header>
      <div className='wrapper'>
         <label htmlFor='heading'> Harvesting Project: </label>
         <h1 id='heading'> {project.name} </h1>
         <nav>
            <button
               type='button'
               onClick={() => setCurrentPage('ProjectsOverview')}
            >
               All projects
            </button>
         </nav>
      </div>
   </header>
   <main>
      <button
         className='main'
         id='new-pile'
         autoFocus={true}
         onClick={() => setCurrentPage('PileForm')}
      >
         + New log pile
      </button>
      <h2>Progress</h2>
      {Number.isFinite(project.targetVolume)
         ? <output
               className='progress-bar'
               htmlFor='volume target'
            >
               <div
                  style={{
                     width: progressFormat.format(Math.min(relativeProgress, 1))
                  }}
               >
                  <div> {progressFormat.format(relativeProgress)} </div>
               </div>
            </output>
         : undefined
      }
      <dl className='project-progress'>
         <dt> Volume: </dt>
         <dd id='volume'>
            <div> {volumeFormat.format(totalVolume)} </div> <div> m³ </div>
         </dd>
         <dt> Value: </dt>
         <dd>
            <div> {valueFormat.format(totalValue)} </div> <div> € </div>
         </dd>
         {Number.isFinite(project.targetVolume)
            ? <>
               <dt> Target: </dt>
               <dd id='target'>
                  <div> {volumeFormat.format(project.targetVolume)} </div>
                  <div> m³ </div>
               </dd>
              </>
            : undefined
         }
      </dl>
      <TargetSetter
         target={project.targetVolume}
         setTarget={setProjectTarget}
      /> <br/>
      <h2>Recorded log piles</h2>
      <ul className='pile-list'>
         {piles
            .sort((a, b) => {
               if (a.recordingTimestamp < b.recordingTimestamp) { return 1; }
               if (a.recordingTimestamp > b.recordingTimestamp) { return -1; }
               return 0;
            })
            .map(pile => (
               <li key={pile.id}>
                  <div> {pile.sortiment} </div>
                  <div> {volumeFormat.format(pile.volume)} m³ </div>
                  <div> {pile.recordingTimestamp.toLocaleDateString()} </div>
                  <button
                     type='button'
                     disabled={pile.user !== user.name}
                     onClick={() => deletePile(pile)}
                  >
                     X
                  </button>
               </li>)
            )}
      </ul>
   </main>
   </>;
};

export default ProjectView;