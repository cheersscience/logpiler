import { useEffect, useState } from "react";

import './Login.css';

import loginService from '../services/login';

const Login = ({ setUser, setCurrentPage }) => {
   const [email, setEmail] = useState('lisa.lumberjane@posteo.net');
   const [password, setPassword] = useState('');
   const [validInput, setValidInput] = useState(false);
   const [errorMessage, setErrorMessage] = useState(null);

   useEffect(() => {
      setValidInput(email !== '' && password !== '');
   }, [email, password]);

   async function handleSubmit(event) {
      event.preventDefault();

      try {
         const response = await loginService.login({ email, password });
         setUser({ name: response.userName, token: response.token });
         setCurrentPage('ProjectsOverview');
      } catch (error) {
         setErrorMessage(`${error.name}: ${error.message}`);
      }
   }

   return <>
      <header>
         <div className='wrapper'>
            <h1> Log In </h1>
         </div>
      </header>
      <main id='login'>
         <form>
            <fieldset>
               <label htmlFor='email'> Email: </label>
               <input
                  id='email'
                  type='email'
                  value={email}
                  onChange={event => setEmail(event.target.value)}
               />
               <label htmlFor='password'> Password: </label>
               <input
                  id='password'
                  type='password'
                  value={password}
                  onChange={event => setPassword(event.target.value)}
               />
               <button
                  type='submit'
                  disabled={!validInput}
                  onClick={handleSubmit}
               >
                  Submit
               </button>
            </fieldset>
         </form>
         {errorMessage &&
            <p className='error-message'> {errorMessage} </p>
         }
      </main>
   </>
};

export default Login;