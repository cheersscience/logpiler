const Footer = () => {

   return <footer>
   <div className='wrapper'>
      <div className='wrapper'>
         LogPiler v0.0.1
      </div>
      <div className='wrapper'>
         by Leon Steinmeier
      </div>
      <div className='wrapper'>
         <a
            target="_blank"
            rel="noreferrer"
            href="https://gitlab.com/cheersscience/logpiler"
         >
            GitLab
         </a>
      </div>
      <div className='wrapper'>
         <a
            target="_blank"
            rel="noreferrer"
            href="https://icons8.com/icon/yNczyL6HIWhg/logs"
         >
            Logs
         </a>
         {' icon by '}
         <a
            target="_blank"
            rel="noreferrer"
            href="https://icons8.com"
         >
            Icons8
         </a>
      </div>
   </div>
   </footer>;
};

export default Footer;