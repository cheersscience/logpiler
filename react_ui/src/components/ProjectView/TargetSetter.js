import { useState } from "react";

const TargetInput = ({ input, setInput }) => {
   return <>
      <input
         id='target-input'
         type='number'
         step='any'
         inputMode='decimal'
         autoFocus
         min={0}
         value={input}
         onChange={event => setInput(event.target.value)}
      />
      {' m³'}
   </>;
};

const TargetSetter = ({ target, setTarget }) => {
   const [focused, setFocused] = useState(false);
   // make sure that "input" becomes an empty string when target is null
   const [input, setInput] = useState(target ? target : '');

   async function handleTargetSubmit(event) {
      event.preventDefault();

      // Both, non-numeric and no input result in an empty string value
      // for "input" and thus inputNumber will always be either a number > 0 or
      // exactly 0 because Number('') returns zero.
      const inputNumber = Number(input);

      if (Number.isFinite(inputNumber) && inputNumber > 0) {
         await setTarget(inputNumber);
      } else {
         await setTarget(null);
         setInput('');
      }

      setFocused(false);
   }

   if (focused) {
      return <form>
         <TargetInput input={input} setInput={setInput} /> <br/>
         <button
            type='submit'
            onClick={handleTargetSubmit}
         >
            {Number.isFinite(Number(input)) && Number(input) > 0
               ? 'Change target'
               : 'Delete target'
            }
         </button>
      </form>;
   } else {
      return <button type='button' onClick={() => setFocused(true)} >
         {input === '' ? 'Set target' : 'Change target'}
      </button>;
   }
};

export default TargetSetter;