import { useState } from 'react';

import projectService from './services/projects';

// Certain elements like e.g. buttons look different from browser to browser and
// operating system to operating system. normalize.css tries to eliminate these
// differences so that the same element has the same appearance in different
// browsers and on varying OSes.
// See http://necolas.github.io/normalize.css/ for details.
import './normalize.css';

import './App.css';

import Footer from './components/Footer';
import Login from './components/Login';
import ProjectsOverview from './components/ProjectsOverview';
import ProjectView from './components/ProjectView';

function App() {
   const [user, setUser] = useState(null);
   const [currentPage, setCurrentPage] = useState('Login');
   const [currentProject, setCurrentProject] = useState(null);
   const [sortiments, setSortiments] = useState([]);
   const [projects, setProjects] = useState([]);

   async function setCurrentProjectTarget(volume) {

      const updatedProject = await projectService.updateProjectTarget(
         currentProject, volume, user
      );
      setCurrentProject(updatedProject);

      const updatedProjects = projects
         .filter(project => project.id !== updatedProject.id)
         .concat(updatedProject);
      setProjects(updatedProjects);
   }

   let resPage;
   switch (currentPage) {
      case 'Login':
         resPage = <Login
            setUser={setUser}
            setCurrentPage={setCurrentPage} />;
         break;
      case 'ProjectsOverview':
         resPage = <ProjectsOverview
            user={user} setUser={setUser}
            setSortiments={setSortiments}
            projects={projects} setProjects={setProjects}
            setCurrentProject={setCurrentProject}
            setCurrentPage={setCurrentPage}
         />;
         break;
      case 'ProjectView':
      case 'PileForm':
         resPage = <ProjectView
            user={user}
            sortiments={sortiments}
            currentPage={currentPage} setCurrentPage={setCurrentPage}
            project={currentProject}
            setProjectTarget={setCurrentProjectTarget}
         />;
         break;
      default:
         return undefined;
   }

   return <> {resPage} <Footer/> </>;
}

export default App;
