import axios from "axios";

const baseUrl = '/api/piles';

async function getPilesOf(projectName, user) {
   try {
      const response = await axios.get(
         `${baseUrl}/ofProject/${encodeURIComponent(projectName)}`,
         { headers: { Authorization: `bearer ${user.token}` } }
      );
      const responseData = response.data.map(pile => ({
         ...pile,
         recordingTimestamp: new Date(pile.recordingTimestamp)
      }));
      return responseData;
   } catch (error) {
      console.error(
         `error while fetching piles of project "${projectName}":
         ${error.response.data.error}`
      );
   }
}

async function add(newPile, user) {
   try {
      const response = await axios.post(
         `${baseUrl}/ofProject/${encodeURIComponent(newPile.project)}`,
         newPile,
         { headers: { Authorization: `bearer ${user.token}` } }
      );
      const responseData = {
         ...response.data,
         recordingTimestamp: new Date(response.data.recordingTimestamp)
      };
      return responseData;
   } catch (error) {
      console.error(
         `error while posting the following pile to project ${newPile.project}:
         ${JSON.stringify(newPile)}`);
   }
}

// deletePile instead of delete because delete is a reserved word
async function deletePile(pile, user) {
   try {
      await axios.delete(
         `${baseUrl}/${pile.id}`,
         { headers: { Authorization: `bearer ${user.token}` } }
      );
   } catch (error) {
      console.error(
         `error while deleting the following pile: ${JSON.stringify(pile)}`);
   }
}

const pileService = { getPilesOf, add, deletePile };
export default pileService;