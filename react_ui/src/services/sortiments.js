import axios from "axios";

const baseUrl = '/api/sortiments';

async function getSortiments(user) {
   try {
      const response = await axios.get(
         `${baseUrl}`,
         { headers: { Authorization: `bearer ${user.token}` } }
      );
      return response.data;
   } catch (error) {
      console.error(
         `error while fetching sortiments: ${JSON.stringify(error)}`
      );
   }
}

const sortimentService = { getSortiments };
export default sortimentService;