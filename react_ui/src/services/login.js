import axios from "axios";

const baseUrl = '/api/login';

async function login(credentials) {
   try {
      const response = await axios.post(`${baseUrl}`, credentials);
      return response.data;
   } catch (error) {
      if (error.response) {
         if (error.response.status === 401) {
            throw new Error(error.response.data.message);
         }
         throw error;
      }
   }
}

const loginService = { login };
export default loginService;