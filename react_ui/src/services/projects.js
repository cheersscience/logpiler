import axios from "axios";

const baseUrl = '/api/projects';

async function getProject(projectName, user) {
   try {
      const response = await axios.get(
         `${baseUrl}/${encodeURIComponent(projectName)}`,
         { headers: { Authorization: `bearer ${user.token}` } }
      );
      const responseData = {
         ...response.data,
         startDate: new Date(response.data.startDate),
         endDate: new Date(response.data.endDate)
      };
      return responseData;
   } catch (error) {
      console.error(
         `error while fetching project "${projectName}":
         ${error.response.data.error}`
      );
   }
}

async function getProjectsOf(user) {
   try {
      const response = await axios.get(
         `${baseUrl}/of/user`,
         { headers: { Authorization: `bearer ${user.token}` } }
      );
      const responseData = response.data.map(project => ({
         ...project,
         startDate: new Date(project.startDate),
         endDate: new Date(project.endDate)
      }));
      return responseData;
   } catch (error) {
      console.error(
         `error while fetching projects of user
         "${user.name}": ${JSON.stringify(error)}`
      );
   }
}

async function updateProjectTarget(project, newTarget, user) {
   try {
      const response = await axios.put(
         `${baseUrl}/${encodeURIComponent(project.name)}`,
         { targetVolume: newTarget },
         { headers: { Authorization: `bearer ${user.token}` } }
      );
      const responseData = {
         ...response.data,
         startDate: new Date(response.data.startDate),
         endDate: new Date(response.data.endDate)
      };
      return responseData;
   } catch (error) {
      console.error(
         `error while updatig project "${project.name}"'s target to
         ${newTarget}`
      );
   }
}

const projectService = { getProject, getProjectsOf, updateProjectTarget };
export default projectService;