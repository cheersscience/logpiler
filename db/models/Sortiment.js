const { DataTypes } = require('sequelize');

const sequelize = require('./Sequelize');

const Sortiment = sequelize.define('Sortiment', {
   name: {
      type: 'character varying',
      primaryKey: true,
      validate: {
         notEmpty: true
      },
      field: 'sortiment_id',
   },
   species: {
      type: 'character varying',
      allowNull: false,
   },
   valuePerVolume: {
      type: DataTypes.DOUBLE,
      allowNull: false,
   },
   volumePerLog: {
      type: DataTypes.DOUBLE,
   },
   numLogsPerPileFrontArea: {
      type: DataTypes.DOUBLE,
   }
}, {
   tableName: 'sortiment',
   underscored: true,
   timestamps: false
});

module.exports = Sortiment;