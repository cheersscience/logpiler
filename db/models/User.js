const sequelize = require('./Sequelize');

const User = sequelize.define('User', {
   name: {
      type: 'character varying',
      primaryKey: true,
      validate: {
         notEmpty: true
      },
      field: 'worker_id',
   },
   email: {
      type: 'character varying',
      unique: true,
      allowNull: false,
      validate: {
         notEmpty: true,
         isEmail: true
      }
   },
   passwordHash:  {
      type: 'character varying',
      allowNull: false,
      validate: {
         notEmpty: true
      }
   }
}, {
   tableName: 'worker',
   underscored: true,
   timestamps: false
});

module.exports = User;