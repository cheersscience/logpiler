const User = require('./User');
const Project = require('./Project');

const sequelize = require('./Sequelize');

const UserXProject = sequelize.define('UserXProject', {
   user: {
      type: 'character varying',
      primaryKey: true,
      references: {
         model: User,
         key: 'name'
      },
      field: 'worker_id',
   },
   project: {
      type: 'character varying',
      primaryKey: true,
      references: {
         model: Project,
         key: 'name'
      },
      field: 'project_id',
   }
}, {
   tableName: 'worker_x_project',
   underscored: true,
   timestamps: false
});

module.exports = UserXProject;