const { DataTypes } = require('sequelize');

const sequelize = require('./Sequelize');

const Project = sequelize.define('Project', {
   name: {
      type: 'character varying',
      primaryKey: true,
      validate: {
         notEmpty: true
      },
      field: 'project_id',
   },
   startDate: {
      type: DataTypes.DATEONLY,
      allowNull: false,
   },
   endDate: {
      type: DataTypes.DATEONLY,
      allowNull: false,
      validate: {
         isAfterStartDate(endDate) {
            if (endDate < startDate) {
               throw new Error(
                  'a project\'s end date was set before its start date');
            }
         }
      }
   },
   targetVolume: {
      type: DataTypes.DOUBLE,
      validate: {
         isGreaterThanZero(volume) {
            if (volume && volume <= 0) {
               throw new Error(
                  `a project\'s target volume was set <= 0 (${volume})`);
            }
         }
      }
   }
}, {
   tableName: 'project',
   underscored: true,
   timestamps: false
});

module.exports = Project;