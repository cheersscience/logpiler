const { DataTypes } = require('sequelize');

const Sortiment = require('./Sortiment');
const Project = require('./Project');
const User = require('./User');

const sequelize = require('./Sequelize');

const Pile = sequelize.define('Pile', {
   id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      autoIncrementIdentity: true,
      field: 'pile_id',
   },
   sortiment: {
      type: 'character varying',
      allowNull: false,
      references: {
         model: Sortiment,
         key: 'name'
      },
      field: 'sortiment_id',
   },
   volume: {
      type: DataTypes.DOUBLE,
      allowNull: false,
      validate: {
         greaterThanZero(volume) {
            if (volume <= 0) {
               throw new Error(
                  `a pile\'s volume set at or below zero (${volume})`);
            }
         }
      }
   },
   project: {
      type: 'character varying',
      allowNull: false,
      references: {
         model: Project,
         key: 'name'
      },
      field: 'project_id',
   },
   user: {
      type: 'character varying',
      allowNull: false,
      references: {
         model: User,
         key: 'name'
      },
      field: 'worker_id',
   },
   location: {
      type: 'point', // DataTypes.GEOMETRY('POINT') want's PostGIS which is
      // currently not installed
   },
   recordingTimestamp: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW,
   }
}, {
   tableName: 'pile',
   underscored: true,
   timestamps: false,
   // sequelize does not support composite foreign keys
});

module.exports = Pile;