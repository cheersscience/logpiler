-- This script inserts data into the database.

DELETE FROM Pile;
DELETE FROM Sortiment;
DELETE FROM Worker_x_Project;
DELETE FROM Project;
DELETE FROM Worker;

INSERT INTO Worker (worker_id, email, password_hash, registration_date) VALUES
    ('Lisa Lumberjane', 'lisa.lumberjane@posteo.net',   '$2b$12$f3YWadakBDjIkdTb2s5CC.GNx4ydROy9cYfB3HPaWMoXaduKnCVVu',
        CURRENT_DATE),
    ('Mirko Masseltof', 'mirko.masseltof@web.de',       '$2b$12$wrr4vXl5PoxGR/5WTyiNP.riAwHIf76kRb.UcobaiexlBMJNoJkYm',
        CURRENT_DATE),
    ('Hannah Hut',      'hannah.hut@gmail.com',         '$2b$12$MW0stDstJu/zhx3hX/AlG.PgZ0IezdDncbLzXkE6PsS.9Ct5IHVbW',
        CURRENT_DATE);

INSERT INTO Project (project_id, start_date, end_date, target_volume) VALUES
    ('Weird/project',   '2070-05-31', '2070-06-10', 42),
    ('Big Wood 2021',   '2021-08-13', '2022-02-28', 20000),
    ('Firewood',        '2021-09-01', '2021-11-30', 600),
    ('Good old days',   '1999-10-15', '2000-02-28', 100000);

INSERT INTO Worker_x_Project (worker_id, project_id) VALUES
    ('Lisa Lumberjane', 'Weird/project'),
    ('Lisa Lumberjane', 'Big Wood 2021'),
    ('Lisa Lumberjane', 'Firewood'),
    ('Lisa Lumberjane', 'Good old days'),
    ('Mirko Masseltof', 'Big Wood 2021');

INSERT INTO Sortiment (
    sortiment_id,
    species,
    value_per_volume,
    volume_per_log,
    num_logs_per_pile_front_area
) VALUES
    ('Beech A',         'Beech',    100,    5 * 3.14 * 0.25^2,  NULL),
    ('Beech B',         'Beech',    80,     4 * 3.14 * 0.20^2,  6.2),
    ('Beech Firewood',  'Beech',    50,     NULL,               NULL),
    ('Oak Commission',  'Oak',      150,    NULL,               NULL),
    ('Pine A',          'Pine',     110,    5 * 3.14 * 0.30^2,  NULL),
    ('Pine B',          'Pine',     85,     3 * 3.14 * 0.27^2,  NULL),
    ('Pine C',          'Pine',     60,     3 * 3.14 * 0.20^2,  6.1),
    ('Spruce B',        'Spruce',   70,     4 * 3.14 * 0.20^2,  6.2),
    ('Spruce C',        'Spruce',   50,     3 * 3.14 * 0.18^2,  6.3);

INSERT INTO Pile (
    sortiment_id,
    volume,
    project_id,
    worker_id,
    location,
    recording_timestamp
) VALUES
    ('Beech Firewood',  99,     'Weird/project',    'Lisa Lumberjane',
        '(13.7, 33)',   '0001-12-24'),
    ('Oak Commission',  5,      'Big Wood 2021',    'Mirko Masseltof',
        '(1, 1)',       CURRENT_TIMESTAMP),
    ('Pine A',          700,    'Big Wood 2021',    'Lisa Lumberjane',
        NULL,           CURRENT_TIMESTAMP),
    ('Pine B',        600,    'Big Wood 2021',    'Lisa Lumberjane',
        NULL,           CURRENT_TIMESTAMP),
    ('Spruce B',        1200,   'Big Wood 2021',    'Lisa Lumberjane',
        NULL,           CURRENT_TIMESTAMP),
    ('Pine A',          5000,   'Good old days',    'Lisa Lumberjane',
        NULL,           '2000-01-02'),
    ('Beech A',         2300,   'Good old days',    'Lisa Lumberjane',
        NULL,           '2000-01-02');