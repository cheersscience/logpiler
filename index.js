const express = require('express');
const config = require('./utils/config');
const morgan = require('morgan');

// Create an express app - this is the progam that will run on the server
const app = express();

// Provide the contents of the build folder (i.e. the front-end code) at the
// base URL
app.use(express.static('build'));

// Parse all incoming data as JSON
app.use(express.json());

// Set up logging with morgan using the tiny configuration
app.use(morgan('tiny'));

// The user router is currently only actived for manually determining password
// hashes
// const usersRouter = require('./routes/users');
// app.use('/api/users', usersRouter);

// Users can login and get a token at this route
const loginRouter = require('./routes/login');
app.use('/api/login', loginRouter);

// Decode and validate the user token in the requests' authorization headers to
// ensure that only logged in users can access the following routes
const decodeUserToken = require('./utils/middleware/decodeUserToken');
app.use('/api/*', decodeUserToken);

// Whenever a user requests data related to a specific project this middleware
// first checks whether the user is assigned to that project
const checkUserIsAssignedToProject = require(
   './utils/middleware/checkUserIsAssignedToProject'
);
app.use(
   [
      '/api/projects/:projectName$',
      '/api/piles/ofProject/:projectName$'
   ],
   checkUserIsAssignedToProject
);

const projectsRouter = require('./routes/projects');
app.use('/api/projects', projectsRouter);

const sortimentsRouter = require('./routes/sortiments');
app.use('/api/sortiments', sortimentsRouter);

const pilesRouter = require('./routes/piles');
app.use('/api/piles', pilesRouter);

// The default handler for every request where the URL did not match any route
app.use((request, response) => {
   response.status(404).send({ error: 'unknown endpoint' });
});

// Handles certain errors in specific ways
function errorHandler(error, request, response, next) {
   console.error(error);

   // Required for cases where an error occurs while the server is sending a
   // response to a client.
   // See section "The default error handler" at:
   // https://expressjs.com/en/guide/error-handling.html
   if (response.headersSent) { return next(error); }

   if (error.name === 'ValidationError') {
      return response.status(400).json({
         message: 'Invalid input!',
         messages: error.errors
      });
   }

   next(error);
}
app.use(errorHandler);

// Start the server
const PORT = config.PORT || 3001;
app.listen(PORT, () => {
   console.log(`Server running on port ${PORT}...`);
});